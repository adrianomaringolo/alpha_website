---
layout: page
title: news.title
namespace: news
permalink: /noticias/
permalink_en: /news/
---
<div class="news">
  <ul>
    {% for post in site.posts %}
      <li>
        <a href="{{ site.baseurl }}{{ post.url }}">
          <div class="thumb-post" style="background-image: url('{{ site.baseurl_root }}/assets/img/news/{{ post.main-img }}');">
            <span class="date">
              {% if site.lang == "pt" %}
                <span class="day">{{ post.date | date: "%d" }}</span>
                <span class="month">{{ post.date | date: "%m" }}</span>
                <span class="year">{{ post.date | date: "%Y" }}</span>
              {% else %}
                <span class="month">{{ post.date | date: "%m" }}</span>
                <span class="day">{{ post.date | date: "%d" }}</span>
                <span class="year">{{ post.date | date: "%Y" }}</span>
              {% endif %}
            </span>
            <span class="title">{{ post.title }}</span>
          </div>
        </a>
      </li>
    {% endfor %}
  </ul>
</div>