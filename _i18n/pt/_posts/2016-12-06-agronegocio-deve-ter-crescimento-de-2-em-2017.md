---
layout: post
title:  "Agronegócio deve ter crescimento de 2% em 2017"
date:   2016-12-06 20:08:00 -0300
lang: pt
main-img: "news-2016-12-06.jpg"
main-img-ref: ''
---

<p class="header">Projeção é Confederação da Agricultura e Pecuária do Brasil (CNA). Participação do setor no PIB deve chegar a 3% em 2016</p>

O agronegócio deve apresentar expansão de 2% em 2017, segundo estimativa da Confederação da Agricultura e Pecuária do Brasil (CNA), divulgada nesta terça-feira (6). Segundo estimativa da confederação, o Produto Interno Bruto (PIB) do agronegócio terá crescimento entre 2,5% a 3%, em 2016.

O setor aumentou a sua participação no PIB de 2015 para este ano, com alteração do percentual de 21,5% para 23%. Para o superintendente técnico da CNA, Bruno Lucchi, a tendência é de continuidade do crescimento do percentual de participação do setor na economia.

Para ele, no próximo ano, o segmento sucroenergético vai expandir, impulsionado pelo aumento de preços do açúcar e etanol. “O café ainda precisa recuperar a produção”, disse. Lucchi acrescentou que o crescimento de outros segmentos, como de proteína animal, vai depender da recuperação da economia para que as pessoas tenham renda para comprar.

O setor agropecuário representa 48% das exportações totais do País, segundo a CNA. Em 2016, os produtos do agronegócio deverão garantir saldo comercial significativo ao País: US$ 72,5 bilhões.

<hr>
Fonte: [Portal Brasil](http://www.brasil.gov.br/economia-e-emprego/2016/12/agronegocio-deve-ter-crescimento-de-2-em-2017)