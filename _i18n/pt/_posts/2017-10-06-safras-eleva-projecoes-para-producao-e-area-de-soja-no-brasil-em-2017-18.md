---
layout: post
title:  "Safras eleva projeções para produção e área de soja no Brasil em 2017/18"
date:   2017-10-06 12:19:00 -0300
lang: pt
main-img: news-2017-10-06.jpg
main-img-ref: ''
---
A consultoria Safras & Mercado elevou nesta sexta-feira sua projeção para a produção de soja na temporada 2017/18 para 114,70 milhões de toneladas, de 113,20 milhões de toneladas na estimativa anterior, de julho.

A projeção para a área também foi elevada, para 35,54 milhões de hectares, de 35,50 milhões de hectares na previsão anterior.

"O início dos trabalhos de plantio no Brasil começa a confirmar nosso sentimento de uma forte expansão da área brasileira de soja nesta nova temporada", afirmou o analista da Safras & Mercado, Luiz Fernando Roque, em nota.

Na temporada passada, o Brasil plantou 33,8 milhões de hectares.

Segundo o analista, a oleaginosa volta a ganhar áreas destinadas ao milho na última safra em praticamente todos os Estados.

No Centro-Oeste e no Sudeste, os produtores confirmam a tendência de centralizar a produção da primeira safra (safra verão) na oleaginosa, enquanto a segunda safra (safrinha) é cada vez mais centralizada no cereal.

"A forte expansão da área de soja nesta nova temporada se ampara principalmente neste fator. Além disso, o fator preço também impulsiona esta transferência de áreas nesta nova safra, com a oleaginosa voltando a remunerar melhor o produtor", completou o analista.

<hr>
Fonte: [Terra](https://www.terra.com.br/economia/safras-eleva-projecoes-para-producao-e-area-de-soja-no-brasil-em-201718,506613d224679c754bc0eb6e4e01a853dm7vw1bk.html)