---
layout: page
title: contact.title
namespace: contact
permalink: /contato/
permalink_en: /contact/
header-image: true
---
<div class="contact">
    <form class="jotform-form" action="https://submit.jotformz.com/submit/70854451269664/" method="post" name="form_70854451269664" id="70854451269664" accept-charset="utf-8"> <input type="hidden" name="formID" value="70854451269664" />
        <div class="form-all">
            <ul class="form-section page-section">
                <li class="form-line jf-required" data-type="control_textbox" id="id_7">
                    <label class="form-label form-label-top form-label-auto" id="label_7" for="input_7"> Nome <span class="form-required"> * </span> </label>
                    <div id="cid_7" class="form-input-wide jf-required">
                        <input type="text" id="input_7" name="q7_name" data-type="input-textbox" class="form-textbox validate[required]" size="20" value="" data-component="textbox" required/>
                    </div>
                </li>
                <li class="form-line jf-required" data-type="control_email" id="id_4">
                    <label class="form-label form-label-top" id="label_4" for="input_4"> E-mail <span class="form-required"> * </span> </label>
                    <div id="cid_4" class="form-input-wide jf-required">
                        <input type="email" id="input_4" name="q4_email" class="form-textbox validate[required, Email]" size="50" value="" data-component="email" required/>
                    </div>
                </li>
                <li class="form-line" data-type="control_textbox" id="id_6">
                    <label class="form-label form-label-top form-label-auto" id="label_6" for="input_6"> Empresa </label>
                    <div id="cid_6" class="form-input-wide jf-required">
                        <input type="text" id="input_6" name="q6_company" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                    </div>
                </li>
                <li class="form-line jf-required" data-type="control_textarea" id="id_5">
                    <label class="form-label form-label-top form-label-auto" id="label_5" for="input_5"> Mensagem <span class="form-required"> * </span> </label>
                    <div id="cid_5" class="form-input-wide jf-required">
                        <textarea id="input_5" class="form-textarea validate[required]" name="q5_message" cols="40"     rows="6" data-component="textarea" required></textarea>
                    </div>
                </li>
                <li class="form-line" data-type="control_button" id="id_2">
                    <div id="cid_2" class="form-input-wide">
                        <div style="text-align:right;" class="form-buttons-wrapper">
                            <button id="input_2" type="submit" class="form-submit-button btn-small" data-component="button"> Enviar minha mensagem </button>
                        </div>
                    </div>
                </li>
                <li>
                    <a target="_blank" href="https://www.jotform.com/signup?utm_source=powered_by_jotform&amp;utm_medium=banner&amp;utm_term=70854451269664&amp;utm_content=powered_by_jotform_text&amp;utm_campaign=powered_by_jotform_signup"
                    class="jf-branding"
                    style="display: block; text-decoration: none; opacity: 0.8; -webkit-font-smoothing: antialiased; color: white; font-size: 13px; text-align: right;">Powered by JotForm</a>
                </li>
                <li style="display:none"> Should be Empty: <input type="text" name="website" value="" /> </li>
            </ul>
        </div>
        <script>
            JotForm.showJotFormPowered = true;
        </script> <input type="hidden" id="simple_spc" name="simple_spc" value="70854451269664" />
        <script type="text/javascript">
            document.getElementById("si" + "mple" + "_spc").value = "70854451269664-70854451269664";
        </script>
    </form>
    <script type="text/javascript">
        JotForm.ownerView = true;
    </script>

    <div class="cv-area">
        <h2><span class="lnr lnr-users"></span>Trabalhe com a gente!</h2>
        <p>Envie seu currículo para <a href="mailto:contratacao@alpha.com.br">contratacao@alpha.com.br</a></p>

        <hr>
        <p>Telefone: +55 (19) 3114-8690</p>

        <hr>

        <strong>Sede</strong><br> Av. Coronel Silva Telles, 977 cj 102<br>Vila Cambuí<br>Campinas/SP
        <br/><br/>
        <a href="https://www.google.com.br/maps/place/Av.+Cel.+Silva+Telles,+977+-+Cambu%C3%AD,+Campinas+-+SP,+13024-001/@-22.8940617,-47.0479989,20z/data=!4m5!3m4!1s0x94c8cf5a84a9530b:0x8dcf84062108f3f5!8m2!3d-22.8942668!4d-47.0477823?hl=pt-BR" class="page-link">Abrir no Google Maps</a>
</div>