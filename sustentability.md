---
layout: page
title: sustentability.title
namespace: sustentability
permalink: /sustentabilidade/
permalink_en: /sustentability/
header-image: true
---

<h3>Responsabilidade Socioambiental</h3>

O compromisso da Alpha com a preservação dos recursos naturais abrange todas as operações da empresa e se consolida em diferentes ações para evitar e minimizar os impactos no meio ambiente.

A empresa realiza investimentos, estabelece metas e define indicadores de acompanhamento com o objetivo de reduzir o consumo de energia e papel, diminuir a geração de resíduos e promover a coleta seletiva.

As atividades operacionais são executadas seguindo diferentes instruções de trabalho que estabelecem procedimentos para controle da emissão de fumaça preta em caminhões da frota contratada, coleta e descarte de embalagens de agrotóxicos utilizados nos armazéns, entre outros aspectos.

Os fornecedores também são orientados e monitorados para que suas atividades causem o menor impacto ambiental possível. A Alpha, por exemplo, só adquire lenha, necessária para a secagem dos grãos, de empresas homologadas capazes de comprovar que a madeira não é originária de áreas de desmatamento.

Além do foco na preservação ambiental, a Alpha busca ter uma participação positiva na sociedade, apoiando iniciativas e projetos de entidades que promovam a educação, a formação e a capacitação profissional de jovens. 

<h3>Saúde e Segurança</h3>

Um dos maiores compromissos da Alpha é a promoção de um ambiente de trabalho seguro, saudável e digno para seus colaboradores. Por isso, a empresa realiza a identificação, o gerenciamento e o monitoramento dos riscos em suas operações e promove a capacitação e conscientização de seus profissionais.

Entre os principais riscos gerenciados pela Alpha estão os relacionados à armazenagem de grãos nas unidades distribuídas pelo Brasil. A empresa segue à risca todas as determinações de normas e legislações vigentes, com o objetivo de evitar acidentes que causem danos aos colaboradores e ao meio ambiente ou possam prejudicar a qualidade dos produtos.

Além de disponibilizar gratuitamente todos os Equipamentos de Proteção Individual (EPIs) aos colaboradores, a Alpha dispõe de equipes de segurança do trabalho e ferramentas específicas que dão suporte à gestão da segurança e saúde em todas as suas unidades.


