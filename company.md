---
layout: page
title: company.title
namespace: company
permalink: /empresa/
permalink_en: /company/
header-image: true
---

A Apha Commodities S.A. chega ao mercado com a proposta de apresentar os valores sólidos de seus investidores, bem como possibilitar a formação de parcerias de longo prazo com fornecedores, tecnólogos agrícolas, novos investidores e clientes.
A Alpha é uma empresa focada na originação e comercialização de soja, milho, pecuária e açúcar, destinados aos m
ercados nacional e internacional.

Com uma estratégia de negócios que envolve as principais operações do setor, em breve estará entre as maiores empresas de comércio agrícola do país.

A empresa nasce grande, com sede na cidade de Campinas (SP), a Alpha possui unidades operacionais em todo o país.
Com uma estratégia de negócios que envolve as principais operações do setor, a empresa é uma das maiores originadoras do país e tem crescido de forma consistente e sustentável.

Além da originação e armazenagem, a empresa é responsável por toda a logística, dispondo de ferramentas de gestão que asseguram o cumprimento dos prazos e a máxima eficiência dos custos. Apoiada por estruturas de investimento, a Alpha também está preparada para auxiliar os produtores com mecanismos inovadores de financiamento.

Com sede na cidade de Campinas (SP), a Alpha possui unidades operacionais nos estados da Bahia, de Goiás, do Paraná, do Mato Grosso, do Mato Grosso do Sul, do Rio Grande do Sul e de São Paulo. Contamos também com profissionais qualificados para dar todo o suporte necessário aos clientes em países como China e Turquia.

A gestão da Alpha é baseada em valores sólidos, que possibilitam a formação de parcerias de longo prazo com fornecedores, desenvolvedores de tecnologias aplicáveis à agricultura, investidores e clientes.
