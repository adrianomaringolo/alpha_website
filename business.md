---
layout: page
title: business.title
namespace: business
permalink: /negocio/
permalink_en: /business/
header-image: true
---

<h3>AGRONEGÓCIOS</h3>

A Alpha Commodities SA nasce como uma das maiores do Brasil em exportação de açúcar, originação e exportação de soja e milho, atuante com uma estratégia de diversificação de serviços e atendimento personalizado para clientes e fornecedores. A empresa se diferencia por sua agilidade nas operações de barter, em que fornece todo o pacote tecnológico de insumos necessários para os produtores agrícolas em troca de uma parcela da produção futura.

O barter dá aos produtores maior segurança durante as safras, já que riscos, custos produtivos e oscilações de mercado são minimizados. A Alpha, por sua vez, amplia continuamente sua capacidade de originação de grãos e ganha agilidade para captar no mercado os recursos necessários para suas operações.

Com essa maneira de fazer negócios, a empresa tem construído relacionamentos duradouros e transparentes, proporcionando a oferta de produtos de qualidade aos clientes e um crescimento econômico sustentável da empresa.

Os produtos comercializados pela Alpha atendem a uma demanda global cada vez maior por alimentos e commodities agrícolas. Os clientes são indústrias de diferentes setores do mercado brasileiro, europeu e asiático. Todos são abastecidos com produtos de alta qualidade e procedência garantida, cultivados em áreas que respeitam a legislação e não promovem o desmatamento do bioma Amazônia.

<h3>Armazenagem</h3>
Com unidades armazenadoras distribuídas em todo o Brasil, a Alpha tem capacidade de armazenagem de mensurada em mais de 60 mil toneladas, além de poder contar com parceiros que proporcionam a mesma qualidade e padrão exigido.

Todas as unidades seguem rigorosamente os mais rígidos critérios de segurança, saúde e responsabilidade ambiental. Os armazéns são equipados com todas as ferramentas necessárias para evitar acidentes e manter a qualidade dos produtos, como sistemas de ventilação e exaustão, combate a incêndios, controle de temperatura para evitar contaminações por micro-organismos etc.

A Alpha realiza ainda investimentos para otimizar o processo de classificação de grãos por meio de treinamentos dos colaboradores e da contratação de empresas especializadas. Essa melhoria tem o objetivo de oferecer a máxima transparência aos clientes e depositantes de acordo com as melhores práticas de mercado.

<h3>Logística</h3>
A Alpha gerencia a logística de seus produtos conforme as necessidades e expectativas de seus clientes, buscando a máxima eficiência na relação entre custos e cumprimento dos prazos. A empresa trabalha com diferentes tipos de modais de transporte, utilizando ferrovias e rodovias até os portos de São Sebastião, Santos, Paranaguá, Rio Grande, São Francisco do Sul e Itaqui.

Por meio do sistema de rastreabilidade, a Alpha e seus clientes têm ferramentas para acompanhar o embarque e recebimento dos produtos em tempo real. Com o apoio dessa tecnologia, as operações são programadas de maneira personalizada para atender aos padrões de logística fracionada exigidos por seus clientes do mercado interno.

Além disso, a Alpha consegue garantir a qualidade de seus produtos na entrega, avaliando-os tanto no momento do embarque quanto do desembarque.

Outro foco da gestão logística é o gerenciamento dos impactos ambientais causados pelo transporte dos produtos, em linha com os padrões estabelecidos pelo Sistema de Gestão Integrado. A empresa utiliza o modal ferroviário para escoar grandes volumes de grãos, açúcar e gado até os portos de embarque, além de uma frota terceirizada de caminhões, assegurando o cumprimento dos contratos no prazo correto.

<h3>Pecuária</h3>
A Alpha Commodities SA atua de forma consistente no ramo de Pecuária, tendo uma vasta capacitação interna, com pessoas importantes e conhecidas no ramo nacional e internacional.

Atualmente dispomos de parceiros que nos provem de recursos apropriados para a exportação de gado, desde a origem até a entrega nos portos. Por esse motivo a empresa constrói relacionamos transparentes e duradouros, que nos ajudam a permanecer entre as maiores exportadoras de boi vivo do Brasil. 

Seguimos o modelo já realizado em Junho de 2017, onde efetivamos a primeira exportação de mais de 12 mil cabeças de boi vivo para a Turquia e obtivemos um feedback positivo dos parceiros, além da ótima notícia de que a finalidade dessa carga foi direcionada a uma causa nobre e servirá de alimento para muitas pessoas necessitadas.

Com esse modelo e seguindo nossa metodologia e capacidade, a Alpha cresce no ramo a fim de chegarmos ao objetivo de estar entre as maiores do mundo.

